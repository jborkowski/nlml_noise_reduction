package algorithm;

import algorithm.beans.RowImage;
import algorithm.beans.Window;
import algorithm.helpers.ImageManager;
import javafx.concurrent.Task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by jborkowski on 6/7/15.
 */
public class Algorithm extends Task<RowImage> {
    int n, m;
    RowImage inImage;
    double sigma = -1.0D;

    public Algorithm (int n, int m, RowImage inImage) {
        this.n = n;
        this.m = m;
        this.inImage = inImage;
    }

    public Algorithm(int n, int m, RowImage inImage, double sigma) {
        this.n = n;
        this.m = m;
        this.inImage = inImage;
        this.sigma = sigma;
    }

    @Override
    protected RowImage call() throws Exception {
        RowImage output = inImage.clone();
        output.setBrightness_offset(inImage.getBrightness_offset());

            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("GMT+0"));

        for (int hImage = 0; hImage < inImage.getH(); hImage++) {
            long startLapTime = System.currentTimeMillis();
            long stopLapTime = 0;
            for (int wImage = 0; wImage < inImage.getW(); wImage++) {

                updateProgress(hImage, inImage.getH());

                if (isCancelled())
                    return null;

                Window window = new ImageManager().getWindow(inImage, wImage, hImage, n, m);
                List<Double> list = ImageManager.getList_IntensityDistances(window, m);

                if(inImage.peek(wImage, hImage) == inImage.getOffset()) {
                    output.set(wImage, hImage, inImage.getOffset());
                    break;
                }
                double val = new ImageManager().maxFun(list, inImage.peek(wImage, hImage), sigma);
                output.set(wImage, hImage, val);
                stopLapTime = System.currentTimeMillis();
            }
            long expectedTime = ( stopLapTime - startLapTime ) * (inImage.getH() - hImage);

            updateMessage("Expected time to end " + df.format(new Date(expectedTime)));
        }
        updateMessage("Noise Reduction Finished");
        return output;
    }


}
