package algorithm.helpers;

import algorithm.beans.Distance;
import algorithm.beans.Neighborhood;
import algorithm.beans.RowImage;
import algorithm.beans.Window;
import flanagan.math.Maximisation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.stream.Collectors;

import static java.lang.Math.*;

/**
 * Created by jborkowski on 6/7/15.
 */
public class ImageManager {

    public static double getSigmaFromBackground (RowImage img, int m) {
        List<Double> array = getArray(img, m, 0);

       final double sum = array.stream()
                   .mapToDouble(p -> p * p)
                   .sum();
       return sum / (2.0D * array.size()) ;
    }

    public static double calcAvgPixelIntensivity (RowImage img, int m, int offset) {
        List<Double> array = getArray(img, m, offset);
        return sqrt(array.stream().mapToDouble(p -> p*p).sum() / array.size());

    }

    private static List<Double> getArray(RowImage in, int m, int offset) {
        List<Double> array = new ArrayList<>();
        for (int x = 0 + offset; x < m + offset; x++) {
            for (int y = 0 + offset; y < m +offset ; y++) {
                array.add(in.peek(x, y));
            }
        }
        return array;
    }

    public Window getWindow (RowImage img, int x, int y, int n, int m){
        /** n - size NB, m - size Window */
        final int radius = (n/2) + (m/2);
        final int sizeArray = (2*(radius)) +1;
        double[][] bufferArray = new double[sizeArray][sizeArray];
        final int xMax = img.getW();
        final int yMax = img.getH();
        int yU=0, xU=0;

        for (int iY = y - radius; iY < y + radius; iY++) {
            for (int iX = x - radius; iX < x + radius; iX++) {
                final int tmpX, tmpY;

                if(iX < 0) tmpX = xMax - abs(iX);
                else if (iX > xMax) tmpX = iX - xMax;
                else tmpX = iX;

                if(iY < 0) tmpY = yMax - abs(iY);
                else if (iY > yMax) tmpY = iY - yMax;
                else tmpY = iY;

                try {
                    bufferArray[xU][yU] = img.peek(tmpX, tmpY);
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Out of Bound ... " + e.getMessage());
                }
                xU++;
            }
            xU =0;
            yU++;
        }

        return new Window(bufferArray, n);
    }

    public static List<Double> getList_IntensityDistances(Window window, int m) {

        final List<Distance> outputList = new ArrayList<>();
        final double[] centralWindow_NH = window.getNeighborhood((m/2)+1, (m/2)+1).getList();
        for (int i = 0;  i < m; i++)
            for (int j = 0;  j < m; j++) {
                final Neighborhood b_NH = window.getNeighborhood(i, j);
                final double d_ij = calcDistance(b_NH.getList(), centralWindow_NH);
                if(d_ij != 0D) {
                    outputList.add(new Distance(b_NH, d_ij));
                }
            }
        return outputList.stream()
                         .sorted((e1, e2) -> Double.compare(e1.getDistance(), e2.getDistance()))
                         .limit(100)
                         .mapToDouble(x -> x.neighborhood.cPoint())
                         .boxed()
                         .collect(Collectors.toList());
    }

    private static double calcDistance(double[] list_A, double[] list_B){
        PrimitiveIterator.OfDouble it_A = Arrays.stream(list_A).iterator();
        return sqrt(Arrays.stream(list_B)
                          .map((p) -> pow((p - it_A.next()), 2))
                          .sum());
    }
    
    public double maxFun(final List<Double> list, final double startValue, final double Ro_2) {
        final Maximisation max = new Maximisation();
        final Function fun = new Function(list, Ro_2);

        final double[] start;
        final double[] step;


        if(Ro_2  == -1.D) {
            start = new double[]{startValue, 100D};
            step = new double[]{10D, 10D};
        } else {
            start = new double[]{startValue-(2*Ro_2)};
            step = new double[]{getPrecision(startValue, 100)};
        }


        max.nelderMead(fun, start, step, 1e-4, 600);
        if(!max.getConvStatus()){

        }

        return max.getParamValues()[0];
    }

    private double getPrecision(double value, int divide) {
        value = abs(value);
        int exp = 0;
        while (value > 1) {
            exp++;
            value /= 10D;
        }
        return pow(10, (exp)) / divide;
    }

    public static double getNormalizationFactor(double[] img, double[] gMatterMask, double[] wMatterMask) {
        final PrimitiveIterator.OfDouble iterGray = Arrays.stream(gMatterMask).iterator();
        final PrimitiveIterator.OfDouble iterWhite = Arrays.stream(wMatterMask).iterator();
        final double aG = Arrays.stream(img).map(i -> i * iterGray.next()).filter(i-> i!=0D).average().getAsDouble();
        final double aW = Arrays.stream(img).map(i -> i * iterWhite.next()).filter(i-> i!=0D).average().getAsDouble();
        return (aW - aG) / (aW + aG);
    }

    public static double calcContrast(double[] denoisedImg, double wFactor, double[] gMatterMask, double[] wMatterMask) {
        final PrimitiveIterator.OfDouble iterGray = Arrays.stream(gMatterMask).iterator();
        final PrimitiveIterator.OfDouble iterWhite = Arrays.stream(wMatterMask).iterator();
        final double aG = Arrays.stream(denoisedImg).map(i -> i * iterGray.next()).filter(i-> i!=0D).average().getAsDouble();
        final double aW = Arrays.stream(denoisedImg).map(i -> i * iterWhite.next()).filter(i-> i!=0D).average().getAsDouble();
        double factor = (aW - aG) / (aW + aG);
        System.out.println(String.format("%.4f", wFactor) + " " + String.format("%.4f", factor) + " is factor W greater? " + (factor > wFactor));
        return  factor / wFactor;
    }

    public static RowImage getSobelGradient(RowImage in, int kernelSize) {
        int origXDim = in.getW();
        int origYDim = in.getH();
        int xDim = origXDim - (kernelSize - 1);
        int yDim = origYDim - (kernelSize - 1);
        int zDim;
        int origSliceSize = origXDim * origYDim;
        double buffer[] = new double[origSliceSize];
        int x;
        int y;
        int sliceSize = xDim * yDim;
        double gx[] = new double[sliceSize];
        double gy[] = new double[sliceSize];

        buffer = in.getArray().clone();
        RowImage out = in.clone();

        if (kernelSize == 3) {

            double[][] sobel_x = new double[][]{{-1,0,1}, {-2,0,2}, {-1,0,1}};
            double[][] sobel_y = new double[][]{{-1,-2,-1}, {0,0,0}, {1,2,1}};




            for (y = 1; y < origYDim - 2; y++) {
                for (x = 1; x < origXDim - 2; x++) {
                    gx[x - 1 + (y - 1)*xDim] = buffer[x+1 + (y-1)*origXDim] - buffer[x-1 + (y-1)*origXDim]
                            + 2.0 * buffer[x+1 + y*origXDim] - 2.0*buffer[x-1 + y*origXDim]
                            + buffer[x+1 + (y+1)*origXDim] - buffer[x-1 + (y+1)*origXDim];
                    gy[x - 1 + (y - 1)*xDim] = buffer[x-1 + (y+1)*origXDim] - buffer[x-1 + (y-1)*origXDim]
                            + 2.0*buffer[x + (y+1)*origXDim] - 2.0*buffer[x + (y-1)*origXDim]
                            + buffer[x+1 + (y+1)*origXDim] - buffer[x+1 + (y-1)*origXDim];
                } // for (x = 1; x < origXDim - 1; x++)
            } // for (y = 1; y < origYDim - 1; y++)
        } // if (kernelSize == 3)
        else if (kernelSize == 5) {
            for (y = 2; y < origYDim - 2; y++) {
                for (x = 2; x < origXDim - 2; x++) {
                    gx[x - 2 + (y - 2)*xDim] = buffer[x+2 + (y-2)*origXDim] + 2.0 * buffer[x+1 + (y-2)*origXDim]
                            - 2.0 * buffer[x-1 + (y-2)*origXDim] - buffer[x-2 + (y-2)*origXDim]
                            + 4.0 * buffer[x+2 + (y-1)*origXDim] + 8.0 * buffer[x+1 + (y-1)*origXDim]
                            - 8.0 * buffer[x-1 + (y-1)*origXDim] - 4.0 * buffer[x-2 + (y-1)*origXDim]
                            + 6.0 * buffer[x+2 + y*origXDim] + 12.0 * buffer[x+1 + y*origXDim]
                            - 12.0 * buffer[x-1 + y*origXDim] - 6.0 * buffer[x-2 + y*origXDim]
                            + 4.0 * buffer[x+2 + (y+1)*origXDim] + 8.0 * buffer[x+1 + (y+1)*origXDim]
                            - 8.0 * buffer[x-1 + (y+1)*origXDim] - 4.0 * buffer[x-2 + (y+1)*origXDim]
                            + buffer[x+2 + (y+2)*origXDim] + 2.0 * buffer[x+1 + (y+2)*origXDim]
                            - 2.0 * buffer[x-1 + (y+2)*origXDim] - buffer[x-2 + (y+2)*origXDim];
                    gy[x - 2 + (y - 2)*xDim] = buffer[x-2 + (y+2)*origXDim] + 2.0 * buffer[x-2 + (y+1)*origXDim]
                            - 2.0 * buffer[x-2 + (y-1)*origXDim] - buffer[x-2 + (y-2)*origXDim]
                            + 4.0 * buffer[x-1 + (y+2)*origXDim] + 8.0 * buffer[x-1 + (y+1)*origXDim]
                            - 8.0 * buffer[x-1 + (y-1)*origXDim] - 4.0 * buffer[x-1 + (y-2)*origXDim]
                            + 6.0 * buffer[x + (y+2)*origXDim] + 12.0 * buffer[x + (y+1)*origXDim]
                            - 12.0 * buffer[x + (y-1)*origXDim] - 6.0 * buffer[x + (y-2)*origXDim]
                            + 4.0 * buffer[x+1 + (y+2)*origXDim] + 8.0 * buffer[x+1 + (y+1)*origXDim]
                            - 8.0 * buffer[x+1 + (y-1)*origXDim] - 4.0 * buffer[x+1 + (y-2)*origXDim]
                            + buffer[x+2 + (y+2)*origXDim] + 2.0 * buffer[x+2 + (y+1)*origXDim]
                            - 2.0 * buffer[x+2 + (y-1)*origXDim] - buffer[x+2 + (y-2)*origXDim];
                } // for (x = 2; x < origXDim - 2; x++)
            } // // for (y = 2; y < origYDim - 2; y++)
        } // else if (kernelSize == 5)


        out.setArray(gy);
        out.setH(yDim);
        out.setW(xDim);

        return out;
    }

    public static double getSharpnessImg(RowImage in) {
        int origXDim = in.getW();
        int origYDim = in.getH();
        int origSliceSize = origXDim * origYDim;
        double buffer[] = new double[origSliceSize];
        int x;
        int y;
        double sharpness = 0.0;
        buffer = in.getArray().clone();

        for (y = 1; y < origYDim - 2; y++) {
            for (x = 1; x < origXDim - 2; x++) {
                final double Gx = buffer[x+1 + (y-1)*origXDim] - buffer[x-1 + (y-1)*origXDim]
                        + 2.0 * buffer[x+1 + y*origXDim] - 2.0*buffer[x-1 + y*origXDim]
                        + buffer[x+1 + (y+1)*origXDim] - buffer[x-1 + (y+1)*origXDim];
                final double Gy = buffer[x-1 + (y+1)*origXDim] - buffer[x-1 + (y-1)*origXDim]
                        + 2.0*buffer[x + (y+1)*origXDim] - 2.0*buffer[x + (y-1)*origXDim]
                        + buffer[x+1 + (y+1)*origXDim] - buffer[x+1 + (y-1)*origXDim];

                final double Wx =  Math.pow(buffer[x+1 + (y)*origXDim] - buffer[x-1 + (y)*origXDim], 2);

                final double Wy =  Math.pow(buffer[x + (y+1)*origXDim] - buffer[x + (y-1)*origXDim], 2);

                sharpness += (Wx * Math.pow(Gx, 2)) + (Wy * Math.pow(Gy, 2));
            }
        }

        return sharpness;
    }
}
