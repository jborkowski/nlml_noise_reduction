package algorithm.helpers;

import JSci.maths.SpecialMath;
import flanagan.math.MaximisationFunction;

import java.util.List;

import static java.lang.Math.log;

/**
 * Created by jborkowski on 6/10/15.
 *
 */
public class Function implements MaximisationFunction {
    private List<Double>  m_i;
    final private double Ro_2;
    final double f1;
    final double logRo_2;

    public Function (List<Double> m_i, double Ro_2) {
        this.m_i = m_i;
        this.Ro_2 = Ro_2;
        f1 = firstComponent();
        logRo_2 = log(Ro_2);
    }

    @Override
    public double function(double[] doubles) {
        final double f2 = secondComponent(doubles[0]);
        final double f3 = thirdComponent(doubles[0]);
        double result = (f1 - f2 + f3);

        return result;
    }

    private double firstComponent () {
        return m_i.stream()
                  .mapToDouble((m) -> log(m) - logRo_2)
                  .sum();
    }

    private double secondComponent (final double A) {
        return m_i.stream()
                  .mapToDouble((m) -> (((m*m) + (A*A)) / (2D * Ro_2)))
                  .sum();
    }
    private double thirdComponent (final double A) {
        //System.out.println(SpecialMath.modBesselFirstZero(5000));
        return m_i.stream()
                  .mapToDouble((m) -> log(SpecialMath.modBesselFirstZero((A * m) / Ro_2)))
                  .sum();
    }

}