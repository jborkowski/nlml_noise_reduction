package algorithm.beans;


import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * Created by jborkowski on 6/5/15.
 */
public class RowImage implements Serializable{

    private static final long serialVersionUID = 1L;

    private double[] array;

    private int w, h;

    private double brightness_offset = 0.0D;

    public void setArray(double[] array) {
        this.array = array;
    }

    public void setW(int w) {
        this.w = w;
    }

    public void setH(int h) {
        this.h = h;
    }

    public double[] getArray(){return array;}

    public int getW() {return w;}

    public int getH() {return h;}

    public double peek(int x, int y) {
        return array[getIdx(x, y)];
    }

    public void set (int x, int y, double val) {
         array[getIdx(x, y)] = val;
    }

    public void img_normalise () {
        if (brightness_offset == 0.0D) {
            double offset = Arrays.stream(array)
                                  .min()
                                  .getAsDouble();
            if (offset < 0.0D)
                brightness_offset = Math.abs(offset)+0.012321;
            array = Arrays.stream(array)
                          .map((p) -> p + brightness_offset)
                          .toArray();
        }

    }

    public void imo_original(){
        array = Arrays.stream(array)
                .map((p) -> p - brightness_offset)
                .toArray();
        brightness_offset = 0.0D;
    }

    public void setBrightness_offset (double val) {
        brightness_offset = val;
    }

    public double getBrightness_offset () {
        return brightness_offset;
    }

    public double getMax () {
        return Arrays.stream(array)
                     .limit(array.length - (4 * this.getW()))
                     .max()
                     .getAsDouble();
    }

    public double getMin () {
        return Arrays.stream(array)
                     .limit(array.length - (4 * this.getW()))
                     .min()
                     .getAsDouble();
    }

    public RowImage clone (){
        try {
            return (RowImage) BeanUtils.cloneBean(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int getIdx (int x, int y) {
        return (y * (w-1)) + x;
    }

    public double getOffset () {
        return brightness_offset;
    }
}
