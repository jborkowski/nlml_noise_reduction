package algorithm.beans;

import java.io.Serializable;

/**
 * Created by jborkowski on 5/13/15.
 */
public class Window implements Serializable {
    final private double[][] windowArray;
    final private int n;

    public Window(double[][] windowArray, int n) {
        this.windowArray = windowArray;
        this.n = n;
    }

    public Neighborhood getNeighborhood(int x_w, int y_w) {
        double[] bufferList = new double[(n * n) - 1];
        int offset_n = n/2;
        x_w += offset_n;
        y_w += offset_n;
        double bufferCenPoint = windowArray[x_w][y_w];
        int i = 0;
            for (int s = 0; s < n; s++) {
                for (int z = 0; z < n; z++) {
                    if (s == offset_n + 1 && z == offset_n + 1) {
                        break;
                    }
                    bufferList[i] = windowArray[-offset_n + s + x_w][-offset_n + z + y_w];
                    i++;
                }
            }
        return new Neighborhood(bufferList, bufferCenPoint);
    }
}
