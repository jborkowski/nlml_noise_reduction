package algorithm.beans;

import java.io.Serializable;

/**
 * Created by jborkowski on 08/12/15.
 */
public class Distance implements Serializable {
    final private double distance;
    final public Neighborhood neighborhood;

    public Distance(Neighborhood neighborhood, double distance) {
        this.neighborhood = neighborhood;
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }
}
