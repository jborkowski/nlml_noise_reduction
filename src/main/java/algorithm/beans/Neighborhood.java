package algorithm.beans;

import java.io.Serializable;

/**
 * Created by jborkowski on 5/14/15.
 */
public class Neighborhood implements Serializable{
    final private double[] neighborhood;
    final private double cPoint;

    public Neighborhood(double[] neighborhood, double cPoint) {
        this.neighborhood = neighborhood;
        this.cPoint = cPoint;
    }

    public double[] getList() {
        return neighborhood;
    }

    public double cPoint() {
        return cPoint;
    }
}
