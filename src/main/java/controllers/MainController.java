package controllers;

import algorithm.Algorithm;
import algorithm.beans.RowImage;
import algorithm.helpers.ImageManager;
import edu.washington.biostr.sig.nifti.AnalyzeNiftiSpmHeader;
import edu.washington.biostr.sig.nifti.NiftiFile;
import helpers.ImageH;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.function.Consumer;

/**
 * Created by jborkowski on 6/3/15.
 */
public class MainController implements Initializable {


    @FXML
    private Canvas preview;

    @FXML
    private ImageView imgP, imgPF;

    @FXML
    private TextField pathEdit, windowSizeEdit, nhEdit, linesEdit, linesSigma, pathDI, pathFNI, pathGM, pathWM;

    @FXML
    private Button processBtn, snrButton, contrastButton, loadBtn, sharpnessButton, errorButton;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Text statusText;

    @FXML
    private Slider slideSlider, windowSlider, levelSlider, windowOutSlider, levelOutSlider;

    @FXML
    private Label actualSlide, actualLevel, actualWindow;

    @FXML
    private CheckBox sigmaCalcWay;

    @FXML
    private TextArea resArea;


    private RowImage imageToDenoise = null;
    private NiftiFile t = null;
    private AnalyzeNiftiSpmHeader hdr;
    private double[] fileBuffer = null;
    Task<RowImage> task;


    //final static String imageSrcPath = "/home/jborkowski/Documents/MSc/file.nii";
    final static String imageSrcPath = "/Users/jborkowski/Documents/images/rf0_noised/3%_78.nii";
    final static String pathToNoised = "/Users/jborkowski/Documents/images/rf0_noised";
    final static String pathToFreeNoiseImg = "/Users/jborkowski/Documents/images/rf0_78.nii";
    final static String pathToGM= "/Users/jborkowski/Documents/images/rf0_78_binnary_mask_gmetter.nii";
    final static String pathToWM = "/Users/jborkowski/Documents/images/rf0_78_binnary_mask_wmetter.nii";


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        linesEdit.disableProperty().bind(sigmaCalcWay.selectedProperty().not());


        linesEdit.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 0) {
                if (!newValue.matches("\\d*"))
                    linesEdit.setText(oldValue);
            }
        });

        linesSigma.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 0) {
                if (!newValue.matches("\\d*"))
                    linesSigma.setText(oldValue);
            }
        });


        //TODO: fix/add: odd, smaller and higher numbers validation!
        nhEdit.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 0) {
                if (!newValue.matches("\\d*")
                        || Integer.parseInt(newValue) % 2 == 0
                        || Integer.parseInt(newValue) > Integer.parseInt(windowSizeEdit.getText()))
                    nhEdit.setText(oldValue);

            }
        });

        windowSizeEdit.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 0) {
                if (!newValue.matches("\\d*")
                        || Integer.parseInt(newValue) % 2 == 0
                        || Integer.parseInt(newValue) < Integer.parseInt(nhEdit.getText()))
                    windowSizeEdit.setText(oldValue);
            }
        });

        pathEdit.setText(imageSrcPath);
        pathDI.setText(pathToNoised);
        pathFNI.setText(pathToFreeNoiseImg);
        pathGM.setText(pathToGM);
        pathWM.setText(pathToWM);
        linesEdit.setText("25");
    }


    public void onClick_loadBtn(ActionEvent event) {
        String path = pathEdit.getText();
        if(t != null && hdr != null) {
            hdr = null;
            t = null;
            fileBuffer = null;
        }

        try {

            t = new NiftiFile(new File(path));
            hdr = hdr.loadHeader(t.getHdr());
            fileBuffer = t.getDoubleArray();
            double max, min;
            int max_slide;
            max = Arrays.stream(fileBuffer).max().getAsDouble();
            min = Arrays.stream(fileBuffer).min().getAsDouble();
            short[] a = hdr.getDim(); // 1 - X, 2 - Y, 3 - Slice (Z);
            max_slide = a[3];
            //??
            setSliders(max, min, max_slide);
            statusText.setText("Load Successful...");

            } catch (IOException e) {
                statusText.setText("File not Load");
            }

    }

    public void onClick_snrButton(ActionEvent event){
       Thread t = new Thread(() -> {
           try {
//               final RowImage freeNoiseImg = getRowImg_String(pathFNI.getText());
//               final Long count = Arrays.stream(freeNoiseImg.getArray()).count();
              // final double meansPixelIntensity = ImageManager.calcAvgPixelIntensivity(freeNoiseImg, Integer.parseInt(linesEdit.getText()), 50);
               resArea.appendText("Calc SNR" + "\n");

               fileEx(pathDI.getText(), file -> {
                   try {
                       final RowImage buffer = getRowImg_File(file);
                       final double sigmaFromNoisedImg = Math.sqrt(ImageManager.getSigmaFromBackground(buffer, Integer.parseInt(linesEdit.getText())));
                       final double meansPixelIntensity = ImageManager.calcAvgPixelIntensivity(buffer, Integer.parseInt(linesEdit.getText()), 50);
                       double SNR = 0.655D * (meansPixelIntensity / sigmaFromNoisedImg);
                       resArea.appendText(file.getName() + " " + SNR + "\n");
                   } catch (IOException e) {
                       System.out.println("Something goes wrong ;P");
                   }
               });

               resArea.appendText("\n");
           } catch (NullPointerException e) {
               System.out.println("I can't find nii files in folder " + e.getMessage());
           }
     });
        t.run();
    }

    public void onClick_sharpnessButton(ActionEvent event) {
        resArea.appendText("Calc sharpness" + "\n");
        fileEx(pathDI.getText(), file -> {
            try {
                final RowImage denoisedImg = getRowImg_File(file);
                final double sharpness = ImageManager.getSharpnessImg(denoisedImg);
                resArea.appendText(file.getName()+" "+sharpness+"\n");
            } catch (IOException e) {

            }
        });
    }

    public void onClick_errorButton(ActionEvent event) {
        resArea.appendText("Calc error " + "\n");
    }

    public void onClick_contrastButton(ActionEvent event) {
        try {
            /** Load binary masks and free Noise img */
            final double[] gMatterMask = getRowImg_String(pathGM.getText()).getArray();
            final double[] wMatterMask = getRowImg_String(pathWM.getText()).getArray();
            final double[] freeNoiseImg = getRowImg_String(pathFNI.getText()).getArray();

            /** Calc 'W' factor */
            final double wFactor = ImageManager.getNormalizationFactor(freeNoiseImg, gMatterMask, wMatterMask);
            resArea.appendText("Calc contrast" + "\n");

            fileEx(pathDI.getText(), file -> {
                try {
                    final double[] denoisedImg = getRowImg_File(file).getArray();
                    final double contrast = ImageManager.calcContrast(denoisedImg, wFactor, gMatterMask, wMatterMask);
                    resArea.appendText(file.getName()+" "+contrast+"\n");
                } catch (IOException e) {
                    System.out.println("Something goes wrong ;P");
                }
            });
            resArea.appendText("\n");
        } catch (NullPointerException e) {
            System.out.println("I can't find nii files in folder " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Something goes wrong ;P");
        }


    }

    private RowImage getRowImg_String(String path) throws IOException {
        final File file = new File(path);
        return getRowImg_File(file);
    }


    private RowImage getRowImg_File(final File file) throws IOException{
        final NiftiFile niftiFile = new NiftiFile(file);
        final AnalyzeNiftiSpmHeader header = AnalyzeNiftiSpmHeader.loadHeader(niftiFile.getHdr());
        return grabSlice(niftiFile.getDoubleArray(), niftiFile.getIndex(0, 0,  0, 0, 0), header.getDim()[1], header.getDim()[2]);
    }

    private RowImage grabSlice(final double[] fileBuffer1, final int sliceIndex, final int sizeX, final int sizeY) {
        final RowImage out = new RowImage();
        double[] bufferImg = new double[sizeX*sizeY];
        System.arraycopy(fileBuffer1, sliceIndex, bufferImg, 0, bufferImg.length);
        out.setArray(bufferImg);
        out.setH(sizeY);
        out.setW(sizeX);
        return out;
    }

    public void onClick_execute(ActionEvent event) {
        if (task != null && task.getState() == Worker.State.RUNNING) {
            task.cancel();
            processBtn.setText("PROCESS");
        } else {
            //Get value window&neighborhood
            final int windowSize = Integer.parseInt(windowSizeEdit.getText());
            final int neighborhoodSize = Integer.parseInt(nhEdit.getText());
            imageToDenoise = easyGrabSlice((int)slideSlider.getValue());
            imageToDenoise.img_normalise();


            if (sigmaCalcWay.isSelected()) {
                double sigma = ImageManager.getSigmaFromBackground(imageToDenoise, Integer.parseInt(linesEdit.getText()));
                task = new Algorithm(neighborhoodSize, windowSize, imageToDenoise, sigma);
            } else {
                task = new Algorithm(neighborhoodSize, windowSize, imageToDenoise);
            }


            statusText.textProperty().bind(task.messageProperty());
            progressBar.progressProperty().bind(task.progressProperty());
            new Thread(task).start();

            task.stateProperty().addListener((observableValue, oldState, newState) -> {
                if (newState == Worker.State.SUCCEEDED) {
                    RowImage img = task.getValue();


                    //Save img.
                    try {
                        OutputStream outputStream = new FileOutputStream("/Users/jborkowski/out.raw");
                        DataOutputStream out = new DataOutputStream(outputStream);

                        for(double val : img.getArray()){
                            int value = new Double(val).intValue();
                          out.writeInt(value);
                        }
                        out.flush();
                        out.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    img.imo_original();
                    showImg(imgPF, img, windowSlider.getValue(), levelSlider.getValue());
                    setOutSliders(img);
                    processBtn.setText("PROCESS");

                }
                if (newState == Worker.State.CANCELLED) {
                    System.out.println("Was Canceled|");
                }
            });
            processBtn.setText("Cancel");
        }
    }

    private void showImg (ImageView img, int slideNum, double window, double level) {
        RowImage bufferImg = easyGrabSlice(slideNum);
        showImg(img, bufferImg, window, level);
    }

    private RowImage easyGrabSlice(int sliceNr) {
        return grabSlice(fileBuffer, t.getIndex(0, 0,  sliceNr, 0, 0), hdr.getDim()[1], hdr.getDim()[2]);
    }

    private void showImg (ImageView img, RowImage rowImage, double window, double level) {
        Image image = ImageH.getGrayscale(ImageH.get8bits(rowImage.getArray(), window, level), rowImage.getW(), rowImage.getH());
        img.setImage(image);
        img.setFitHeight(rowImage.getH());
        img.setFitWidth(rowImage.getW());
    }

    private void setSliders (double max, double min, int maxSlide) {
        //Sliders!!

        slideSlider.setMin(0);
        slideSlider.setMax(maxSlide-1);
        slideSlider.setValue(0);
        actualSlide.setText(String.valueOf((int)slideSlider.getValue()));
        slideSlider.setShowTickLabels(false);
        slideSlider.setShowTickMarks(false);
        levelSlider.setMin(min + 0.01D);
        levelSlider.setMax(max);
        levelSlider.setValue((Math.abs(min) + Math.abs(max)) / 2 );
        levelSlider.setShowTickLabels(false);
        levelSlider.setShowTickMarks(false);
        actualWindow.setText(String.format("%.3f", windowSlider.getValue()));
        windowSlider.setMin( 0.02D);
        windowSlider.setMax(max);
        windowSlider.setValue(0.27);
        windowSlider.setShowTickLabels(false);
        windowSlider.setShowTickMarks(false);
        actualLevel.setText(String.format("%.3f", levelSlider.getValue()));


        slideSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            showImg(imgP, (int) slideSlider.getValue(), windowSlider.getValue(), levelSlider.getValue());
            actualSlide.setText(String.valueOf((int)slideSlider.getValue()));
        });

        windowSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            showImg(imgP, (int) slideSlider.getValue(), windowSlider.getValue(), levelSlider.getValue());
            actualWindow.setText(String.format("%.3f", windowSlider.getValue()));
        });

        levelSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            showImg(imgP, (int) slideSlider.getValue(), windowSlider.getValue(), levelSlider.getValue());
            actualLevel.setText(String.format("%.3f", levelSlider.getValue()));
        });
    }

    private void setOutSliders (RowImage img){
        levelOutSlider.setMin(img.getMin() + 0.01D);
        levelOutSlider.setMax(img.getMax());
        levelOutSlider.setValue((Math.abs(img.getMin()) + Math.abs(img.getMax())) / 2);
        levelOutSlider.setShowTickLabels(false);
        levelOutSlider.setShowTickMarks(false);
        windowOutSlider.setMin(0.02D);
        windowOutSlider.setMax(img.getMax());
        windowOutSlider.setValue(0.27);
        windowOutSlider.setShowTickLabels(false);
        windowOutSlider.setShowTickMarks(false);

        windowOutSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            showImg(imgPF, img, windowOutSlider.getValue(), levelOutSlider.getValue());

        });

        levelOutSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            showImg(imgPF, img, windowOutSlider.getValue(), levelOutSlider.getValue());

        });
    }

    private void fileEx(String pathToFolder, Consumer<File> toDO ) {
        final File folder = new File(pathToFolder);
        final File[] files = folder.listFiles();
        for (final File file : files) {
            if (file.getName().equals(".DS_Store")) continue;
            toDO.accept(file);
        }
    }
}
