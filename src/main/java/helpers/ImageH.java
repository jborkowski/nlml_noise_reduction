package helpers;

import javafx.embed.swing.SwingFXUtils;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;
import java.util.Arrays;

/**
 * Created by jborkowski on 6/17/15.
 */
public class ImageH {
    public static javafx.scene.image.Image getGrayscale(byte[] buffer, int width, int height) {
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        int[] nBits = { 8 };
        ColorModel cm = new ComponentColorModel(cs, nBits, false, true,
                Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
        SampleModel sm = cm.createCompatibleSampleModel(width, height);
        DataBufferByte db = new DataBufferByte(buffer, width * height);
        WritableRaster raster = Raster.createWritableRaster(sm, db, null);
        BufferedImage result = new BufferedImage(cm, raster, false, null);

        return SwingFXUtils.toFXImage(result, null);
    }
    public static byte[] get8bits(double[] buffer, double window, double level) {
        byte[] output = new byte[buffer.length];
        double min, max;
        min = level - (window/2);
        max = level + (window/2);

        int[] array = Arrays.stream(buffer)
                .parallel()
                .map(p -> {
                    if (p > max)
                        return max;
                    else if (p < min)
                        return min;
                    else
                        return p;
                })
                .mapToInt(p -> (byte) ((p + Math.abs(min)) / (Math.abs(min) + max) * 255))
                .toArray();
        for (int i = 0; i < array.length; i++) {
            output[i] = (byte) array[i];
        }


        return output;

    }
}
